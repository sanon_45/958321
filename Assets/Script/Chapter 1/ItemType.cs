using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Sanon.GameDev3.Chapter1
{
    public enum ItemType
    {
        COIN,
        BIGCOIN,
        POWERUP ,
        POWERDOWN ,
        SMALLCOIN ,
        SUPERCOIN,
    }
}

