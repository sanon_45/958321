using UnityEngine;
using UnityEngine.InputSystem;

namespace Sanon.Gamedev.chapter2.InputSystem
{
    public class InputSystem_ControlObjectMovementOnXYPlaneUsingArrowKeys : MonoBehaviour
    {
        public float m_MovementStep;
        
            // Use this for initialization
            void Start()
            {
                
            }

            void Update()
            {
                Keyboard keyboard = Keyboard.current;
                
                //GetKey
                if (keyboard[Key.LeftArrow].isPressed)
                {
                    this.transform.Translate(-m_MovementStep , 0, 0);
                }
                else if (keyboard[Key.RightArrow].isPressed)
                {
                    this.transform.Translate(m_MovementStep , 0, 0);
                }
                else if (keyboard[Key.UpArrow].isPressed)
                {
                    this.transform.Translate(0, m_MovementStep , 0);
                }
                else if (keyboard[Key.DownArrow].isPressed)
                {
                    this.transform.Translate(0, -m_MovementStep , 0);
                }
            }
    }
}